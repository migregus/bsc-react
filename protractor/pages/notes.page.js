module.exports = function () {
    this.addNewButton = element(by.css('[class="fa fa-plus-square-o"]'));
    this.input = element(by.name('note'));
    this.saveButton = element(by.buttonText('Save'));
    this.firstNoteEditAnchor = element.all(by.css('[class="fa fa-pencil-square-o"]')).first();
    this.firstNoteDeleteAnchor = element.all(by.css('[class="fa fa-trash-o"]')).first();
    this.yesButton = element(by.buttonText('Yes'));
};