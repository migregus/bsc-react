var Navigation = require('../pages/navigation.page.js');
var NotesPage = require('../pages/notes.page');

describe('Note list', function () {

    var notesPage = new NotesPage();

    describe('Navigate to notes', function () {
        it('Should navigate to note page', function () {
            var navigation = new Navigation();
            browser.get('http://localhost:9000');
            browser.sleep(500);
            navigation.notesLink.click();
            browser.sleep(500);
            expect(browser.getCurrentUrl()).toMatch('http://localhost:9000/notes');
        })
    })

    describe('Create a new note', function () {
        it('Should click to icon to open modal', function () {
            browser.get('http://localhost:9000/notes');
            notesPage.addNewButton.click();
            browser.sleep(500);
        })
        it('Should enter a new note and click save', function () {
            notesPage.input.sendKeys('This is new note');
            browser.sleep(500);
            notesPage.saveButton.click();
            browser.sleep(1000);
        })
        it('Should find new note', function () {
            let removedRow = element(by.cssContainingText('.list-group-item', 'This is new note'));
            expect(browser.isElementPresent(removedRow)).toBe(true);
        })
    })

    describe('Edit existing note', function () {
        it('Should navigate to edit note page', function () {
            browser.get('http://localhost:9000/notes');
            browser.sleep(500);
            notesPage.firstNoteEditAnchor.click();
            browser.sleep(500);
        })
        it('Should replace note and save', function () {
            notesPage.input.clear();
            notesPage.input.sendKeys('This is new note');
            browser.sleep(500);
            notesPage.saveButton.click();
            browser.sleep(1000);
        })
        it('Should find updated note', function () {
            let firstRow = element.all(by.css('[class="list-group-item"]')).first();
            expect(firstRow.getText()).toMatch('This is new note');
        })
    })

    describe('Remove existing note', function () {
        it('Should click on delete and not find note on page.', function () {
            browser.get('http://localhost:9000/notes');
            browser.sleep(500);
            notesPage.firstNoteDeleteAnchor.click();
            browser.sleep(500);
            notesPage.yesButton.click();
            browser.sleep(500);
            let removedRow = element(by.cssContainingText('.ist-group-item', 'Jogging in park'));
            browser.sleep(500);
            expect(browser.isElementPresent(removedRow)).toBe(false);
        })
    })
})
