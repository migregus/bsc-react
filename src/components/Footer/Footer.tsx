import React from 'react';

const Footer: React.FC = () => {
  return <div>
    <hr/>
    <p className="font-weight-lighter text-center">&copy;	2019 Michal Gregus</p>
  </div>;
}

export default Footer;
