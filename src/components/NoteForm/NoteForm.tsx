import React, { useEffect, useState } from 'react';
import { Button, Form, FormFeedback, FormGroup, Input, Label } from 'reactstrap';
import { withTranslation, WithTranslation } from 'react-i18next';

interface Props extends WithTranslation {
  title: string;
  label: string;
  onSave: (title: string) => void;
  onCancel: () => void;
}

const NoteForm: React.FC<Props> = ({ t, title, label, onSave, onCancel }) => {

  const [value, setValue] = useState(title);
  const [touched, setTouched] = useState(false);

  useEffect(() => {
      setValue(title);
    }, [title]);

  const handleChange = (event: React.FormEvent<HTMLInputElement>) => {
    const target = event.currentTarget;
    setValue(target.value ? target.value : '');
    setTouched(true);
  };

  return <Form>
    <FormGroup>
      <Label for="exampleText">{label}</Label>
      <Input invalid={touched && value === ''} type="textarea" name="note" value={value} onChange={handleChange}/>
      <FormFeedback>{t('note.validation')}</FormFeedback>
    </FormGroup>
    <div className="d-flex justify-content-end">
      <Button onClick={() => onSave(value)} color="primary">{t('common.save')}</Button>
      <Button onClick={onCancel}>{t('common.cancel')}</Button>
    </div>
  </Form>;
};

export default withTranslation()(NoteForm);
