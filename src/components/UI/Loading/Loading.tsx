import React from 'react';
import { Spinner } from 'reactstrap';

const Loading: React.FC = () => {
  return <div className="d-flex justify-content-center">
    <Spinner style={{ width: '3rem', height: '3rem' }} type="grow" />
  </div>;
};

export default Loading;
