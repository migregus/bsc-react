import React from 'react';

interface Props {
  title: string;
}

const PageHeader: React.FC<Props> = ({ title, children }) => {
  return <>
    <h4 className="d-flex justify-content-between align-items-center">
      {title}
      {children}
    </h4>
    <hr/>
    </>
};

export default PageHeader;
