import React, { useState } from 'react';
import { Note } from '../../model/Note';
import { Button, Modal, ModalBody } from 'reactstrap';
import { Link } from 'react-router-dom';
import { withTranslation, WithTranslation } from 'react-i18next';

interface Props extends WithTranslation {
  note: Note;
  onDelete: (id: number) => void;
}

const NoteThumbnail: React.FC<Props> = ({ t, note, onDelete }) => {

  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  const handleRemove = () => {
    onDelete(note.id);
    toggle();
  };

  let confirm = null;
  if (modal) {
    confirm = <Modal isOpen={modal} toggle={toggle}>
      <ModalBody>
        <div>{t('note.remove.body')}</div>
        <div className="d-flex justify-content-end">
          <Button color="primary" onClick={handleRemove}>{t('common.yes')}</Button>{' '}
          <Button color="secondary" onClick={toggle}>{t('common.no')}</Button>
        </div>
      </ModalBody>
    </Modal>;
  }

  return <div className="d-flex align-items-center justify-content-between">
    <span>{note.title}</span>
    <span>
      <Button to={`notes/${note.id}`} tag={Link} color="link"><i className="fa fa-pencil-square-o" aria-hidden="true"/></Button>
      <Button onClick={toggle} color="link"><i className="fa fa-trash-o" aria-hidden="true"/></Button>
    </span>
    {confirm}
  </div>;
};

export default withTranslation()(NoteThumbnail);