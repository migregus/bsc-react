import React, { useState } from 'react';
import { NavLink, Link } from 'react-router-dom';
import { Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink as RNavLink } from 'reactstrap';
import { WithTranslation, withTranslation } from 'react-i18next';

const Navigation: React.FC<WithTranslation> = ({ t, i18n }) => {

  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);
  const setTranslation = (lng: string) => i18n.changeLanguage(lng);
  const lng = i18n.language;

  return <div>
    <Navbar color="dark" dark expand="md">
      <NavbarBrand href="/">BSC</NavbarBrand>
      <NavbarToggler onClick={toggle} />
      <Collapse isOpen={isOpen} navbar>
        <Nav className="mr-auto" navbar>
          <NavItem>
            <RNavLink tag={NavLink} exact to="/">{t('navigation.home')}</RNavLink>
          </NavItem>
          <NavItem>
            <RNavLink tag={NavLink} exact to="/notes">{t('navigation.notes')}</RNavLink>
          </NavItem>
          <NavItem>
            <RNavLink tag={NavLink} exact to="/requirements">{t('navigation.requirements')}</RNavLink>
          </NavItem>
          <NavItem>
            <RNavLink href="http://private-9aad-note10.apiary-mock.com/">{t('navigation.rest')}</RNavLink>
          </NavItem>
        </Nav>
      </Collapse>
      <Nav className="ml-auto" navbar>
        <NavItem>
          <RNavLink onClick={() => setTranslation('en')} active={lng === 'en'} to={''} tag={Link}>{t('navigation.lng.en')}</RNavLink>
        </NavItem>
        <NavItem>
          <RNavLink onClick={() => setTranslation('cz')} active={lng === 'cz'} to={''} tag={Link}>{t('navigation.lng.cz')}</RNavLink>
        </NavItem>
      </Nav>
    </Navbar>
  </div>;
};

export default withTranslation()(Navigation);
