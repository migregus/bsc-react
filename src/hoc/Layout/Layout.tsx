import React from 'react';
import Navigation from '../../components/Navigation/Navigation';
import Footer from '../../components/Footer/Footer';

const Layout: React.FC = (props) => {
  return (
    <>
      <header><Navigation /></header>
      <div className="main">
        <main className="d-flex flex-column">{props.children}</main>
        <footer><Footer/></footer>
      </div>
    </>
  );
};

export default Layout;
