import i18n  from "i18next";
import { initReactI18next } from "react-i18next";

// the translations
// (tip move them in a JSON file and import them)
const resources = {
  en: {
    translation: {
      common: {
        cancel: 'Cancel',
        no: 'No',
        notFound: 'Not found',
        save: 'Save',
        yes: 'Yes',
      },
      navigation: {
        home: 'Home',
        notes: 'Notes',
        requirements: 'Requirements',
        rest: 'Rest API',
        lng: {
          en: 'EN',
          cz: 'CZ',
        },
      },
      home: {
        welcome: 'Welcome to BSC',
        description: 'This is small sample application.',
        button: 'Get started'
      },
      notes: {
        title: 'Notes',
        createNote: 'Create Note',
      },
      note: {
        title: 'Note',
        edit: 'Edit Note',
        validation: 'This field is required',
        remove: {
          title: 'Delete Note',
          body: 'Do you want to delete note?',
        }
      }
    }
  },
  cz: {
    translation: {
      common: {
        cancel: 'Zrušení',
        no: 'Ne',
        notFound: 'Nenalezeno',
        save: 'Uložit',
        yes: 'Ano',
      },
      navigation: {
        home: 'Domov',
        notes: 'Poznámky',
        requirements: 'Požadavky',
        rest: 'Rest API',
        lng: {
          en: 'EN',
          cz: 'CZ',
        },
      },
      home: {
        welcome: 'Vítejte v BSC',
        description: 'Toto je malá ukázková aplikace.',
        button: 'Začít'
      },
      notes: {
        title: 'Poznamky',
        createNote: 'Vytvorit poznamku',
      },
      note: {
        title: 'Poznámky',
        edit: 'Upravit poznámku',
        validation: 'Toto pole je povinné',
        remove: {
          title: 'Smazat poznámku',
          body: 'Chcete smazat poznámku?',
        }
      },
    }
  }
};

i18n
  .use(initReactI18next)
  .init({
    resources,
    lng: "en",
    keySeparator: '.',

    interpolation: {
      escapeValue: false
    }
  });

export default i18n;