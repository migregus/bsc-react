import React from 'react';

const Requirements: React.FC = () => {
  return <div className="container">
    <h4>ÚKOL PRO UCHAZEČE O POZICI FRONTEND DEVELOPER V BSC</h4>
    <p>Připravte jednoduchou aplikaci v AngularJs (nebo ReactJS) tak, aby se dala po naklonování repository
      a s nainstalovaným node.js nainstalovat a spustit.</p>
    <p><strong>Předpřipravený server BSC s REST API, které bude aplikace používat:</strong></p>
    <p>Root URL: http://private-9aad-note10.apiary-mock.com/ (případně použijte REST api podle uvážení:
      https://www.firebase.com/ , http://jsonplaceholder.typicode.com/, nebo vlastní řešení)</p>
    <p><strong>Metody:</strong></p>
    <p>GET /notes<br />
      GET /notes/id<br />
      POST /notes<br />
      PUT /notes/id<br />
      DELETE /notes/id<br /></p>

    <p><strong>Funkční požadavky:</strong></p>
    <p>Po instalaci a spuštění se po zadání localhost:9000 objeví stránka se seznamem s poznámkami.
      Je možné zobrazit detail, editovat, smazat a vytvořit novou poznámku. (Apiary Mock bude vracet
      stále stejná data, předmětem úkolu je volat správné metody)
      V aplikaci bude možné měnit EN/CZ jazyk.</p>
    <p><strong>Nefunkční požadavky:</strong></p>
    <p>GUI dle vlastního návrhu, použití Bootstrapu a LESS/SASS/Stylus preferováno.
      Kód by měl být ES5+ JS s použitim novějších API jako Promise, Array extras..bonus pokud by byl
      použit Typescript<br/>
      Instalace závislostí pomocí Bower<br/>
      Build pomoci gulp nebo grunt, případně webpack nebo browserify.<br/>
      Tamplaty v js.<br/>
      Použít ui-router (nebo https://github.com/rackt/react-router).<br />
      Alespoň jeden základní protractor test.<br />
      Kód vyvíjejte do github/bitbucket veřejného repository, v souboru README.md popište instrukce pro<br />
      instalaci a spuštění aplikace a testu, a pošlete URL emailem.</p>
  </div>
};

export default Requirements;
