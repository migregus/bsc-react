import React from 'react';
import { WithTranslation, withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

const Home: React.FC<WithTranslation> = ({ t }) => {
  return <div  className="d-flex flex-column justify-content-center flex-grow-1 text-center">
    <h1 className="display-1">{t('home.welcome')}</h1>
    <p>{t('home.description')}</p>
    <span className="d-flex justify-content-center">
      <Link to="/notes" className="btn btn-outline-primary">{t('home.button')}</Link>
    </span>
  </div>;
};

export default withTranslation()(Home);