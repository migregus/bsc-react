import React, { useEffect, useState } from 'react';
import PageHeader from '../../../components/PageHeader/PageHeader';
import { WithTranslation, withTranslation } from 'react-i18next';
import NoteForm from '../../../components/NoteForm/NoteForm';
import { RouteComponentProps, withRouter } from 'react-router';
import { RootState } from '../../../store/state';
import { getNote, NotesState, updateNote } from '../../../store/notes';
import { connect } from 'react-redux';
import { RequestState } from '../../../model/RequestState';
import { Note } from '../../../model/Note';
import Loading from '../../../components/UI/Loading/Loading';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

interface OwnProps {
  noteState: NotesState;
  getNote: typeof getNote;
  updateNote: typeof updateNote;
}

type Props = OwnProps & WithTranslation & RouteComponentProps;

const NoteEdit: React.FC<Props> = ({ t, getNote, noteState, updateNote, history, match }) => {

  const [note, setNote] = useState<Note | undefined>(undefined);

  useEffect(() => {
      const id = (match.params as any).id;
      if (noteState.state === RequestState.SUCCESS && noteState.data) {
        const note = noteState.data.find(note => note.id === +id);
        if (note) {
          setNote(note);
        } else {
          notify();
          history.push('/notes');
        }
      }
      if (noteState.state === RequestState.NOT_ASKED) {
        getNote(id);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [noteState, match.params]);

  const navigateToNotes = () => {
    history.push('/notes');
  };

  const handleSave = (title: string) => {
    if (note) {
      updateNote(note.id, title);
    }
    navigateToNotes();
  };

  const notify = () => toast(t('common.notFound'));

  let editNote = <Loading />;
  if (noteState.state === RequestState.SUCCESS && note) {
    editNote = <div className="container">
      <PageHeader title={t('note.title')}/>
      <NoteForm title={note ? note.title : ''} label={t('note.edit')}
                onCancel={navigateToNotes} onSave={handleSave}/>
    </div>;
  }

  return editNote;
};

const mapStateToProps = (state: RootState) => {
  return {
    noteState: state.notes,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    getNote: (id: number) => dispatch(getNote(id)),
    updateNote: (id: number, title: string) => dispatch(updateNote(id, title))
  };
};

const note = withRouter(NoteEdit);
export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(note));
