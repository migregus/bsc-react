import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { RootState } from '../../store/state';
import { createNote, deleteNote, getNotes, NotesState } from '../../store/notes';
import { RequestState } from '../../model/RequestState';
import Loading from '../../components/UI/Loading/Loading';
import { Button, ListGroup, ListGroupItem, Modal, ModalBody } from 'reactstrap';
import { WithTranslation, withTranslation } from 'react-i18next';
import NoteThumbnail from '../../components/NoteThumbnail/NoteThumbnail';
import PageHeader from '../../components/PageHeader/PageHeader';
import NoteForm from '../../components/NoteForm/NoteForm';

interface Props extends WithTranslation {
  noteState: NotesState;
  getNotes: typeof getNotes;
  createNote: typeof createNote;
  deleteNote: typeof deleteNote;
}

const Notes: React.FC<Props> = ({ t, noteState, getNotes, createNote, deleteNote }) => {

  const [modal, setModal] = useState(false);

  useEffect(() => {
    getNotes();
  },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []);

  const toggle = () => setModal(!modal);

  const handleSave = (title: string) => {
    createNote(title);
    toggle();
  };

  let notes = <Loading />;
  if (noteState.state === RequestState.SUCCESS) {
    let list;
    if (noteState.data && noteState.data.length > 0) {
      list = noteState.data.map(note => <ListGroupItem key={note.id}>
        <NoteThumbnail note={note} onDelete={deleteNote}/>
      </ListGroupItem>);
    } else {
      list = <div>No Data</div>;
    }
    notes = <ListGroup>{list}</ListGroup>;
  }
  if (noteState.state === RequestState.FAILURE) {
    notes = <span>ERROR</span>;
  }
  return <div className="container">
    <PageHeader title={t('notes.title')}>
      <Button color="link" size="lg" onClick={toggle}>
        <i className="fa fa-plus-square-o" aria-hidden="true"/>
      </Button>
    </PageHeader>
    {notes}
    <Modal isOpen={modal} toggle={toggle}>
      <ModalBody>
        <NoteForm title="" label={t('notes.createNote')} onCancel={toggle} onSave={handleSave}/>
      </ModalBody>
    </Modal>
  </div>;
};

const mapStateToProps = (state: RootState) => {
  return {
    noteState: state.notes,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    getNotes: () => dispatch(getNotes()),
    createNote: (title: string) => dispatch(createNote(title)),
    deleteNote: (id: number) => dispatch(deleteNote(id)),
  };
};

const withTrans = withTranslation()(Notes);
export default connect(mapStateToProps, mapDispatchToProps)(withTrans);
