import axios from 'axios';

import { Note } from '../model/Note';

const dsc = axios.create({
  baseURL: 'http://private-9aad-note10.apiary-mock.com'
});

export function fetchNotes(): Promise<Note[]> {
  return dsc.get<Note[]>('/notes')
    .then(response => response.data);
}

export function fetchNote(id: number): Promise<Note> {
  return dsc.get<Note>(`/notes/${id}`)
    .then(response => response.data);
}

export function addNote(title: string): Promise<Note> {
  return dsc.post<Note>('/notes', { title: title })
    .then(response => response.data);
}

export function editNote(id: number, title: string): Promise<Note> {
  return dsc.put<Note>(`/notes/${id}`, { title: title })
    .then(response => response.data);
}

export function removeNote(id: number): Promise<void> {
  return dsc.delete(`/notes/${id}`);
}
