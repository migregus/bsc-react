import { NotesState } from './notes';

export interface RootState {
  notes: NotesState;
}