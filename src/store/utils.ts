import { set } from 'lodash/fp';
import { RequestState } from '../model/RequestState';

export const updateState = (state: any, action: any, newState: RequestState) => {
  let s = state;
  s = set(['state'], newState, s);
  if (action.data) {
    s = set(['data'], action.data, s);
  }
  if (action.error) {
    s = set(['data'], undefined, s);
    s = set(['error'], action.error, s);
  } else {
    s = set(['error'], undefined, s);
  }
  return s;
};
