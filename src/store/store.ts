import { applyMiddleware, combineReducers, compose, createStore } from 'redux';

import notes from './notes';
import { RootState } from './state';
import thunk from 'redux-thunk';

const initialState: Partial<RootState> = {};

const rootReducer = combineReducers({
  notes: notes,
});

// const composeEnhancers = process.env.NODE_ENV === 'development' ? (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null || compose;
let composeEnhancers = null;
if (process.env.NODE_ENV === 'development') {
  composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
} else {
  composeEnhancers = compose;
}
const store = createStore(rootReducer, initialState, composeEnhancers(
  applyMiddleware(thunk)
));

export default store;
