/**
 * @see inpired by [https://github.com/erikras/ducks-modular-redux]
 */
import { RequestState } from '../../model/RequestState';
import { Note } from '../../model/Note';
import { updateState } from '../utils';
import { Dispatch } from 'redux';
import { addNote, editNote, fetchNote, fetchNotes, removeNote } from '../../services/notes';
import * as _ from 'lodash';

// Actions
const PREFIX = 'bsc/notes';

export const START = PREFIX + '/START';
export const SUCCESS = PREFIX + '/SUCCESS';
export const FAILURE = PREFIX + '/FAILURE';

export const READ_START = PREFIX + '/read/START';
export const READ_SUCCESS = PREFIX + '/read/SUCCESS';
export const READ_FAILURE = PREFIX + '/read/FAILURE';

export const CREATE_START = PREFIX + '/create/START';
export const CREATE_SUCCESS = PREFIX + '/create/SUCCESS';
export const CREATE_FAILURE = PREFIX + '/create/FAILURE';

export const UPDATE_START = PREFIX + '/update/START';
export const UPDATE_SUCCESS = PREFIX + '/update/SUCCESS';
export const UPDATE_FAILURE = PREFIX + '/update/FAILURE';

export const DELETE_START = PREFIX + '/delete/START';
export const DELETE_SUCCESS = PREFIX + '/delete/SUCCESS';
export const DELETE_FAILURE = PREFIX + '/delete/FAILURE';

export interface NotesState {
  state: RequestState;
  data?: Note[];
  error?: string;
}

const initialState: NotesState = {
  state: RequestState.NOT_ASKED,
};

// Reducer
export default function reducer(state = initialState, action: {type: string, data?: any}) {
  switch (action.type) {
    case START:
    case READ_START:
    case CREATE_START:
    case UPDATE_START:
    case DELETE_START:
      return updateState(state, action, RequestState.PENDING);
    case SUCCESS:
      if (state.data) {
        const updated = [...state.data];
        const result = _.unionBy(updated, action.data, 'id');
        return {
          ...state,
          state: RequestState.SUCCESS,
          data: result.sort((a,b) => a.id - b.id)
        }
      }
      return updateState(state, action, RequestState.SUCCESS);
    case READ_SUCCESS: {
      const data = state.data ? [...state.data, action.data] : [action.data];
      return {
        ...state,
        state: RequestState.SUCCESS,
        data: data
      };
    }
    case CREATE_SUCCESS: {
      if (state.data) {
        const newId = Math.max.apply(Math, state.data.map(note => note.id));
        return {
          ...state,
          state: RequestState.SUCCESS,
          data: [...state.data, { id: newId +1 , title: action.data }]
        };
      } else {
        return {
          ...state,
          state: RequestState.SUCCESS,
          data: [action.data]
        };
      }
    }
    case UPDATE_SUCCESS: {
      if (state.data) {
        return {
          ...state,
          state: RequestState.SUCCESS,
          data: state.data.map(note =>
            note.id === action.data.id
            ? { ...note, title: action.data.title }
            : note)
        }
      }
      return state;
    }
    case DELETE_SUCCESS: {
      if (state.data && action.data) {
        return {
          ...state,
          state: RequestState.SUCCESS,
          data: state.data.filter(note => note.id !== action.data)
        };
      }
      return state.data;
    }
    case FAILURE:
    case READ_FAILURE:
    case CREATE_FAILURE:
    case UPDATE_FAILURE:
    case DELETE_FAILURE:
      return updateState(state, action, RequestState.FAILURE);
    /* istanbul ignore next */
    default: return state;
  }
}

// Action Creators
// Resources Actions
export function actionFetchStart() {
  return { type: START };
}

export function actionFetchSuccess(data: any) {
  return { type: SUCCESS, data };
}

export function actionFetchFailure(error: string) {
  return { type: FAILURE, error };
}

export function actionReadFetchStart() {
  return { type: READ_START };
}

export function actionReadFetchSuccess(data: any) {
  return { type: READ_SUCCESS, data };
}

export function actionReadFetchFailure(error: string) {
  return { type: READ_FAILURE, error };
}

export function actionCreateStart() {
  return { type: CREATE_START };
}

export function actionCreateSuccess(data: any) {
  return { type: CREATE_SUCCESS, data };
}

export function actionCreateFailure(error: string) {
  return { type: CREATE_FAILURE, error };
}

export function actionUpdateStart() {
  return { type: UPDATE_START };
}

export function actionUpdateSuccess(data: any) {
  return { type: UPDATE_SUCCESS, data };
}

export function actionUpdateFailure(error: string) {
  return { type: UPDATE_FAILURE, error };
}

export function actionDeleteStart() {
  return { type: DELETE_START };
}

export function actionDeleteSuccess(data: any) {
  return { type: DELETE_SUCCESS, data };
}

export function actionDeleteFailure(error: string) {
  return { type: DELETE_FAILURE, error };
}

// side effects, only as applicable
// e.g. thunks, epics, etc
export function getNotes() {
  return async (dispatch: Dispatch<any>) => {
    dispatch(actionFetchStart());
    try {
      const response = await fetchNotes();
      dispatch(actionFetchSuccess(response));
    } catch (e) {
      dispatch(actionFetchFailure(e.toString()));
    }
  };
}

export function getNote(id: number) {
  return async (dispatch: Dispatch<any>) => {
    dispatch(actionReadFetchStart());
    try {
      const response = await fetchNote(id);
      dispatch(actionReadFetchSuccess(response));
    } catch (e) {
      dispatch(actionReadFetchFailure(e.toString()));
    }
  };
}

export function createNote(title: string) {
  return async (dispatch: Dispatch<any>) => {
    dispatch(actionCreateStart());
    try {
      await addNote(title); // ignore response, because it returns only note 3
      dispatch(actionCreateSuccess(title));
    } catch (e) {
      dispatch(actionCreateFailure(e.toString()));
    }
  }
}

export function updateNote(id: number, title: string) {
  return async (dispatch: Dispatch<any>) => {
    dispatch(actionUpdateStart());
    try {
      await editNote(id, title); // ignore response, because it always return note 2
      dispatch(actionUpdateSuccess({id: id, title: title}));
    } catch (e) {
      dispatch(actionUpdateFailure(e.toString()));
    }
  }
}

export function deleteNote(id: number) {
  return async (dispatch: Dispatch<any>) => {
    dispatch(actionDeleteStart());
    try {
      await removeNote(id);
      dispatch(actionDeleteSuccess(id));
    } catch (e) {
      dispatch(actionDeleteFailure(e.toString()));
    }
  }
}