import reducer, {CREATE_SUCCESS, DELETE_SUCCESS, SUCCESS, UPDATE_SUCCESS} from "./index";
import {RequestState} from "../../model/RequestState";

describe('notes reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            state: RequestState.NOT_ASKED,
            data: undefined,
            error: undefined,
        });
    });

    it('should store notes', () => {
        expect(reducer({
            state: RequestState.NOT_ASKED,
            data: undefined,
            error: undefined,
        }, {
            type: SUCCESS,
            data: [{id: 1, title: "Jogging in park"}, {id: 2, title: "Pick-up posters from post-office"}],
        })).toEqual({
            state: RequestState.SUCCESS,
            data: [{id: 1, title: "Jogging in park"}, {id: 2, title: "Pick-up posters from post-office"}],
            error: undefined,
        });
    });

    it('should add note', () => {
        expect(reducer({
            state: RequestState.SUCCESS,
            data: [{id: 1, title: "Jogging in park"}, {id: 2, title: "Pick-up posters from post-office"}],
            error: undefined,
        }, {
            type: CREATE_SUCCESS,
            data: "New test note",
        })).toEqual({
            state: RequestState.SUCCESS,
            data: [
                {id: 1, title: "Jogging in park"},
                {id: 2, title: "Pick-up posters from post-office"},
                {id: 3, title: "New test note"},
            ],
            error: undefined,
        });
    });

    it('should update note', () => {
        expect(reducer({
            state: RequestState.SUCCESS,
            data: [
                {id: 1, title: "Jogging in park"},
                {id: 2, title: "Pick-up posters from post-office"},
                {id: 3, title: "New test note"},
            ],
            error: undefined,
        }, {
            type: UPDATE_SUCCESS,
            data: {id: 3, title: "Updated note"},
        })).toEqual({
            state: RequestState.SUCCESS,
            data: [
                {id: 1, title: "Jogging in park"},
                {id: 2, title: "Pick-up posters from post-office"},
                {id: 3, title: "Updated note"},
            ],
            error: undefined,
        });
    });

    it('should delete note', () => {
        expect(reducer({
            state: RequestState.SUCCESS,
            data: [
                {id: 1, title: "Jogging in park"},
                {id: 2, title: "Pick-up posters from post-office"},
                {id: 3, title: "Updated note"},
            ],
            error: undefined,
        }, {
            type: DELETE_SUCCESS,
            data: 3,
        })).toEqual({
            state: RequestState.SUCCESS,
            data: [{id: 1, title: "Jogging in park"}, {id: 2, title: "Pick-up posters from post-office"}],
            error: undefined,
        });
    });
});
