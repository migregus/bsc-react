export enum RequestState {
  NOT_ASKED = 'NOT_ASKED',
  PENDING = 'PENDING',
  SUCCESS = 'SUCCESS',
  FAILURE = 'FAILURE',
}
