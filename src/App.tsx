import React from 'react';
import { Route, Switch, Redirect, BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux'

import Layout from './hoc/Layout/Layout';
import Home from './containers/Home/Home';
import Notes from './containers/Notes/Notes';
import Requirements from './containers/Requirements/Requirements';
import store from './store/store';
import NoteEdit from './containers/Notes/NoteEdit/NoteEdit';
import { toast } from 'react-toastify';

toast.configure({
  autoClose: 2000,
  draggable: false,
});

const App: React.FC = () => {

  return (
    <div className="App">
      <Provider store={store}>
        <BrowserRouter>
            <Layout>
              <Switch>
                <Route path="/requirements" exact component={Requirements}/>
                <Route path="/notes/:id" exact component={NoteEdit}/>
                <Route path="/notes" exact component={Notes}/>
                <Route path="/" exact component={Home} />
                <Redirect to="/" />
              </Switch>
            </Layout>
        </BrowserRouter>
      </Provider>
    </div>
  );
}

export default App;
